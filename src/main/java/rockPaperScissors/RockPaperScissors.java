package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();

    }



    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> YesOrNo = Arrays.asList("y", "n");
    Random rand= new Random();

    public String random_choice(){
        return rpsChoices.get(rand.nextInt(3));
    }


    public boolean is_winner( String choice1, String choice2) {

        if (choice1.equals( "paper")){
            return (choice2.equals ( "rock"));
        }else if (choice1.equals( "scissors")){
            return (choice2.equals ( "paper" ));
        }else {
            return (choice2.equals ( "scissors" ));
        }

    }

    public String user_choice(){

        while (true) {
            System.out.println ("Your choice (Rock/Paper/Scissors)?");
            String human_choice = sc.next();
            if (validate_input ( human_choice, rpsChoices )) {
                return human_choice;
            } else {
                System.out.println ( "I don't understand " + human_choice + ". Try again" );

            }
            break;
        }
        return "";
    }

    public String continue_playing(){


        while (true) {
            System.out.println ("Do you wish to continue playing? (y/n)?");
            String continue_answer = sc.next();
            if (validate_input ( continue_answer, YesOrNo )) {
                return continue_answer;
            } else {
                System.out.println ( "I don't understand " + continue_answer + ".Try again\"" );
            }
            break;
        }
        return "";

    }

    public boolean validate_input(String input, List<String> valid_input){

        return valid_input.contains ( input );
        }



    public void run() {
         //TODO: Implement Rock Paper Scissors

        while (true){
            System.out.println ("Let's play round "+ roundCounter);
            String human_choice = user_choice ();
            String computer_choice = random_choice();

            String choice_string = "Human chose " +  human_choice + " computer chose " + computer_choice + ".";


            if(is_winner ( human_choice,computer_choice )){
                System.out.println (choice_string+ "Human wins.");
                humanScore= humanScore + 1;
            }else if (is_winner(computer_choice, human_choice)){
                System.out.println (choice_string + " Computer wins.");
                computerScore = computerScore + 1;
            }else{
                System.out.println (choice_string + " It's a tie");
            }
            System.out.println ("Score: human " + computerScore + ", computer " + humanScore);

           String continue_answer = continue_playing ();
            if (continue_answer.equals ( "n" ))
                break;

        }
        System.out.println ("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
